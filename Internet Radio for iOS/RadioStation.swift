//
//  RadioStation.swift
//  Internet Radio for iOS
//
//  Created by Felix v. Oertzen on 27.03.19.
//  Copyright © 2019 Felix v. Oertzen. All rights reserved.
//

/*
 Internet Radio for iOS
 Copyright (C) 2019  Felix v. O.
 felix@von-oertzen-berlin.de
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 */


import Foundation

class RadioStation:Equatable {
    
    private let name: String;
    private let url: URL;
    
    init(name:String,url:String) {
        self.name = name;
        self.url = URL(string:url)!;
    }
    
    func getStationName() -> String {
        return name;
    }
    
    func getStationUrl() -> URL {
        return self.url;
    }
    
    func getStationUrlHumanReadable() -> String {
        return getStationUrl().absoluteString
    }
    
    func getString() -> String {
        return getStationName() + ": " + getStationUrl().absoluteString;
    }
    
    public static func ==(lhs: RadioStation, rhs: RadioStation) -> Bool{
        return lhs.getStationUrl() == rhs.getStationUrl() && lhs.getStationName() == rhs.getStationName()
    }
}
