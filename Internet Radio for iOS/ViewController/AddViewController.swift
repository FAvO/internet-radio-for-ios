//
//  SecondViewController.swift
//  Internet Radio for iOS
//
//  Created by Felix v. Oertzen on 26.03.19.
//  Copyright © 2019 Felix v. Oertzen. All rights reserved.
//

/*
 Internet Radio for iOS
 Copyright (C) 2019  Felix v. O.
 felix@von-oertzen-berlin.de
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 */


import UIKit

class AddViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var urlField: UITextField!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    @IBAction func cancelTouched(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    @IBAction func nameFieldChanged(_ sender: Any) {
        checkSaveableState();
    }
    
    @IBAction func urlFieldChanged(_ sender: Any) {
        checkSaveableState();
    }
    
    @IBAction func clearButton(_ sender: Any) {
        urlField.text = "";
        nameField.text = "";
    }
    
    @IBAction func saveButtonTouched(_ sender: Any) {
        saveStation();
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        nameField.delegate = self;
        urlField.delegate = self;
        
        nameField.becomeFirstResponder();
    }
    
    private func checkSaveableState(){
        saveButton.isEnabled = ( !(nameField.text?.isEmpty ?? true) && !(urlField.text?.isEmpty ?? true) );
        
    }
    
    private func saveStation(){
        let nameText = nameField.text;
        let urlText  = urlField.text;
        
        if  let name = nameText,
            let url = urlText {
            if (!url.starts(with: "http")) {
                let alertController = UIAlertController(title:  NSLocalizedString("FAILURE", comment: ""), message: NSLocalizedString("WRONG_URL_SYNTAX", comment: ""), preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: NSLocalizedString("GOTIT", comment: ""), style: .default, handler: nil)
                alertController.addAction(defaultAction)
                
                present(alertController, animated: true, completion: nil)
            } else {
                let stationManagment = StationManagment()
                stationManagment.loadStations()
                stationManagment.addStation(station: RadioStation(name: name, url: url))
                stationManagment.saveStations()
                if let tabBarController = self.tabBarController {
                    tabBarController.selectedIndex = 0
                }
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}

