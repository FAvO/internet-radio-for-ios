//
//  SettingsController.swift
//  Internet Radio for iOS
//
//  Created by Felix v. Oertzen on 26.03.19.
//  Copyright © 2019 Felix v. Oertzen. All rights reserved.
//

import UIKit
/*
 Internet Radio for iOS
 Copyright (C) 2019  Felix v. O.
 felix@von-oertzen-berlin.de
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
*/
class SettingsController: UITableViewController {

    private var settings:SettingsManagment!;
    

    @IBAction func mobileDataSwitched(_ sender: UISwitch) {
        settings.setMobileDataEnabled(isEnabled: false)
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        settings = SettingsManagment();
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
}
