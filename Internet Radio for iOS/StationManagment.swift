//
//  StationManagment.swift
//  Internet Radio for iOS
//
//  Created by Felix v. Oertzen on 27.03.19.
//  Copyright © 2019 Felix v. Oertzen. All rights reserved.
//
/*
 Internet Radio for iOS
 Copyright (C) 2019  Felix v. O.
 felix@von-oertzen-berlin.de
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 */


import Foundation
class StationManagment {
    let defaults:UserDefaults;
    var stationList: [RadioStation];
    
    init() {
        defaults = UserDefaults.standard
        stationList = [RadioStation]();
    }
    
    func loadStations() {
        let stations    = defaults.dictionary(forKey: "stations")
        let order       = defaults.array(forKey: "order")
        var radios      = [RadioStation]()
        
        if let s = stations, let o = order {
            for r in o {
                let a = r as! String;
                radios += [RadioStation(name: a, url: s[a] as! String)]
            }
        }
      
        self.stationList = radios;
    }
    
    func saveStations() {
        var dict = [String:String]();
        var ordr = [String]();
        
        for station in stationList {
            dict[station.getStationName()] = station.getStationUrl().absoluteString;
            ordr += [station.getStationName()];
        }
        
        defaults.set(dict, forKey: "stations")
        defaults.set(ordr, forKey: "order")
        defaults.synchronize()
    }
    
    func getStations() -> [RadioStation] {
        return stationList;
    }

    func addStation(station: RadioStation) {
        stationList += [station];
    }
    
    func deleteStation(position: Int) {
        stationList.remove(at: position)
    }
    
    func moveStation(From from:Int, To to:Int) {
        let station = stationList[from]
        stationList.remove(at: from)
        stationList.insert(station, at: to)
    }
}
