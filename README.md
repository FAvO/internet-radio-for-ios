[![Swift](https://img.shields.io/badge/using-Swift-green.svg?logo=swift&color=fa7e4e)](https://www.apple.com/swift/)
[![License](https://img.shields.io/badge/License-GNU%20General%20Public%20License%20v3.0-green.svg?logo=gnu)](https://www.gnu.org/licenses/gpl-3.0.en.html)

# Internet Radio for iOS
It's a simple app for listening to internet radio stations.

## How can I get it?
Actually you need to compile it yourself; I did not published it yet because of the high costs for Apple's Development program.

## Can I support this project?
I would like you to support me.
For example, for adding new features or designing a beautiful app icon.

## What's about Android?
There is also an app developed by me, which will be published on gitlab.com/FAvO soon.
